# -*- coding: utf-8 -*-

# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    'name': 'Le Filament Deactivate Menus',
    'description':
    """
    Le Filament module for making menus deactivable
    ===============================================
     """,
    'version': '10.0.1.0.0',
    'depends': ['base'],
    'installable': True,
    'auto_install': False,
    'author': 'LE FILAMENT',
    'contributors': [
        'Rémi Cazenave <remi@le-filament.com>',
    ],
    'data': [
        "views/lf_show_active_menus.xml",
    ],
}
