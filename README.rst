.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


=====================
Le Filament - Deactivate Menus
=====================

This module provides a button on each menu (to be accessed in developper mode) to (de)activate it 


Usage
=====

On each menu form view you find a new button to (de)activate the menu.
The menu is then (not) shown anymore.

Credits
=======

Contributors ------------

* Remi Cazenave <remi@le-filament.com>


Maintainer ----------

.. image:: https://le-filament.com/img/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament